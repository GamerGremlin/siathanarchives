﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Pushable : MonoBehaviour {

    public void Push(float speed, Vector3 direction)
    {
        transform.Translate(direction * speed * Time.deltaTime);
    }
}

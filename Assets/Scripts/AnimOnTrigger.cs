﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimOnTrigger : MonoBehaviour {

    public Animator animController;
    public string animName;
    public string tagCheck;

	// Update is called once per frame
	void OnTriggerEnter (Collider hit)
    {
		if(hit.gameObject.tag == tagCheck)
        {
            animController.Play(animName);
        }
	}
}

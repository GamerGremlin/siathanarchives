﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(CharacterController))]
[DisallowMultipleComponent]
public class FirstPersonMove : MonoBehaviour
{

    private CharacterController charControl;
    private Camera cam;


    private float baseSpeed;
    private float vertForce;

    [Header("Movement Options")]

    [Tooltip("Movement Speed")]
    public float speed = 2f;
    [Tooltip("Jump Force")]
    public float jumpForce = 3f;
    [Tooltip("Virtual gravity force")]
    public float gravity = 1.5f;
    [Tooltip("Terminal Velocity")]
    public float maxVerticle = 10f;
    [Tooltip("Turning Speed")]
    public float rotSpeed = 1f;
    [Tooltip("Multiplier applied to speed when sprinting")]
    public float sprintMulti = 1.5f;
    [Tooltip("Leeway for when the character is considered on the ground")]
    public float groundedDistance = 0.75f;

    [Header("Control Keys")]
    public KeyCode moveLeft = KeyCode.A;
    public KeyCode moveRight = KeyCode.D;
    public KeyCode moveForward = KeyCode.W;
    public KeyCode moveBackward = KeyCode.S;
    public KeyCode jump = KeyCode.Space;
    [Tooltip("Set to NONE for no sprinting")]
    public KeyCode sprint = KeyCode.LeftShift;

    [Header("Camera Settings")]
    [Tooltip("Maximum downwards angle. 90 is straight down.")]
    [Range(0, 90)]
    public float ClampAngleLower = 90f;
    [Tooltip("Maximum upwards angle. 90 is straight up.")]
    [Range(0, 90)]
    public float ClampAngleUpper = 90f;


    private void Awake()
    {
        charControl = GetComponent<CharacterController>();
        cam = GetComponentInChildren<Camera>();
        baseSpeed = speed;
    }

    void Update()
    {
        if (Input.GetKey(sprint))
        {
            speed = baseSpeed * sprintMulti;
        }
        else
        {
            speed = baseSpeed;
        }


        if (Input.GetKey(moveForward))
        {
            charControl.Move(transform.forward * speed * Time.deltaTime);
        }
        if (Input.GetKey(moveBackward))
        {
            charControl.Move(-transform.forward * speed * Time.deltaTime);
        }
        if (Input.GetKey(moveLeft))
        {
            charControl.Move(-transform.right * speed * Time.deltaTime);
        }
        if (Input.GetKey(moveRight))
        {
            charControl.Move(transform.right * speed * Time.deltaTime);
        }
        transform.Rotate(transform.up, rotSpeed * Input.GetAxis("Mouse X") * Time.deltaTime);


        vertForce -= (gravity * Time.deltaTime);
        vertForce = Mathf.Clamp(vertForce, -maxVerticle, maxVerticle);

        if (Input.GetKeyDown(jump) && IsGrounded())
        {
            vertForce = jumpForce;
        }

        charControl.Move(transform.up * vertForce * Time.deltaTime);

        float camTilt = cam.transform.localEulerAngles.x + -Input.GetAxis("Mouse Y");

        if ((camTilt > ClampAngleLower) && (camTilt < 180))
            camTilt = ClampAngleLower;
        else if ((camTilt < (360 - ClampAngleUpper)) && (camTilt > 180))
            camTilt = (360 - ClampAngleUpper);

        //Change the camera rotation to the clamped rotation
        cam.transform.localEulerAngles = new Vector3(camTilt, 0, 0);

    }

    public bool IsGrounded()
    {
        //Check if the player is grounded, taking into account the tolerance
        return Physics.Raycast(transform.position, -transform.up, groundedDistance);
    }
}

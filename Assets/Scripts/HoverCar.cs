﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
[RequireComponent(typeof(Rigidbody))]
public class HoverCar : Interactable {

    [Tooltip("How quickly the car will accelerate")]
public float speed;
    [Tooltip("How quickly the car will rotate")]
public float turnSpeed;
    [Header("Rigidbody Settings")]
public Rigidbody carBody;
public Transform rayStart;
    [Tooltip("How strong the push up from the ground is")]
    public float hoverforce;
    [Tooltip("How high the car will hover")]
    public float hoverHeight;
    [Tooltip("How much the car will resist turning via the rigidbody")]
    public float resistForce;
    [Tooltip("Deacceleration force from friction. Only applied when no movement key is pressed")]
    public float decayForce;
    [Header("Movement Controls")]
public KeyCode left = KeyCode.A;
public KeyCode right = KeyCode.D;
public KeyCode forward = KeyCode.W;
public KeyCode backward = KeyCode.S;
private RaycastHit hit;
    // Use this for initialization
    void Start () 
	{
        if (carBody == null)
        {
            carBody = GetComponent<Rigidbody>();
        }
        if (rayStart == null)
        {
            rayStart = transform.Find("RayStart");
        }
        active = true;
	}
	
	// Update is called once per frame
	void Update () 
	{
        if (active)
        {
            if (Input.GetKey(left))
            {
                transform.RotateAround(transform.position, transform.up, -turnSpeed * Time.deltaTime);
            }
            if (Input.GetKey(right))
            {
                transform.RotateAround(transform.position, transform.up, turnSpeed * Time.deltaTime);
            }
        }
        float angleZ = transform.localEulerAngles.z;
        angleZ = (angleZ > 180) ? angleZ - 360 : angleZ;
        float angleX = transform.localEulerAngles.x;
        angleX = (angleX > 180) ? angleX - 360 : angleX;
        if (angleX >= 0.1f || angleX <= -0.1f || angleZ <= -0.1f || angleZ >= 0.1f)
        {
            Quaternion targetRotation;
            if (hit.transform != null)
            {
                targetRotation = Quaternion.Euler(hit.transform.eulerAngles.x, transform.rotation.eulerAngles.y, 0);
            }
            else
            {
                targetRotation = Quaternion.Euler(0, transform.rotation.eulerAngles.y, 0);
            }
            transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, 0.65f * Time.deltaTime);
        }
    }

	void FixedUpdate()
	{
        if (active)
        {
            if (Input.GetKey(forward))
            {
                Vector3 applyForce = transform.forward * speed;
                if (transform.InverseTransformDirection(carBody.velocity).z < 0)
                {
                    applyForce = applyForce * 2;
                }
                carBody.AddForce(applyForce, ForceMode.Acceleration);
            }
            if (Input.GetKey(backward))
            {
                Vector3 applyforce = -transform.forward * speed;
                if (transform.InverseTransformDirection(carBody.velocity).z > 0)
                {
                    applyforce = applyforce * 2;
                }
                carBody.AddForce(applyforce, ForceMode.Acceleration);
            }
        }
        Ray ray = new Ray(rayStart.position, -transform.up);

        if (Physics.Raycast(ray, out hit, hoverHeight))
        {
            float propHeight = (hoverHeight - hit.distance) / hoverHeight;
            Vector3 applyForce = Vector3.up * propHeight * hoverforce;
            carBody.AddForce(applyForce, ForceMode.Force);
        }
        if (transform.InverseTransformDirection(carBody.velocity).x > 0)
        {
            Vector3 applyForce = -transform.right * resistForce;
            carBody.AddForce(applyForce, ForceMode.Acceleration);
        }
        if (transform.InverseTransformDirection(carBody.velocity).x < 0)
        {
            Vector3 applyForce = transform.right * resistForce;
            carBody.AddForce(applyForce, ForceMode.Acceleration);
        }
        if (carBody.velocity.z > 0f && !Input.GetKey(forward) && !Input.GetKey(backward))
        {
            carBody.velocity *= 0.99f;
        }
        if (carBody.velocity.z < 0f && !Input.GetKey(backward) && !Input.GetKey(forward))
        {
            carBody.velocity *= 0.99f;
        }
    }

    public override void Interact(Interact sender)
    {
        if(active)
        {
            OnDeactivate();
        }
        else
        {
            OnActivate();
        }
    }

    public override void OnActivate()
    {

    }

    public override void OnDeactivate()
    {
        
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ship : MonoBehaviour {

    private bool inverted;

    public float speedMax;
    public float speedMin;

    public float speed;

    public float turnSpeed;

    public float maxPitch;
    public float minPitch;

    public float speedDifference;
    public KeyCode speedUp = KeyCode.R;
    public KeyCode speedDown = KeyCode.F;
    public KeyCode up = KeyCode.W;
    public KeyCode down = KeyCode.S;
    public KeyCode left = KeyCode.A;
    public KeyCode right = KeyCode.D;
    public KeyCode fire = KeyCode.Space;
    public KeyCode rollLeft = KeyCode.Q;
    public KeyCode rollRight = KeyCode.E;
    public KeyCode invert = KeyCode.I;
    public KeyCode flatSpeed = KeyCode.H;

    public GameObject projectile;
    public float fireForce;
    public float fireDelay;
    public Material fireYes;
    public Material fireNo;
    public GameObject fireIndicator;
    public AudioSource laser;


    private float ticker;

    private Rigidbody rb;
    private Renderer indicatorRenderer;
    private AudioSource sound;

	// Use this for initialization
	void Start ()
    {
        rb = GetComponent<Rigidbody>();
        indicatorRenderer = fireIndicator.GetComponent<Renderer>();
        sound = GetComponent<AudioSource>();
        speed = 0;
        ticker = fireDelay;
        inverted = true;
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (speed == 0f)
        {
            sound.volume = 0;
        }
        else
        {
            sound.volume = 0.5f;
        }
        if (!(ticker >= fireDelay))
        {
            ticker += Time.deltaTime;
        }
        if (speed < speedMin)
        {
            speed = speedMin;
        }
        if (speed > speedMax)
        {
            speed = speedMax;
        }
#if UNITY_STANDALONE_WIN
        if (Input.GetKey(flatSpeed) || Input.GetKey(KeyCode.Joystick1Button3))
#elif UNITY_PS4
        if (Input.GetKey(KeyCode.Joystick1Button3))
#endif
        {
            speed = 0f;
        }
#if UNITY_STANDALONE_WIN
        if(Input.GetKeyDown(invert) || Input.GetKeyDown(KeyCode.Joystick1Button9))
#elif UNITY_PS4
        if(Input.GetKeyDown(KeyCode.Joystick1Button4))
#endif
        {
            inverted = !inverted;
        }
#if UNITY_STANDALONE_WIN
        if ((Input.GetKey(speedUp) || Input.GetKey(KeyCode.Joystick1Button7)) && (speed < speedMax))
#elif UNITY_PS4
        if ((Input.GetAxis("Joy1Axis8") > 0.1f) && (speed < speedMax))
#endif
        {
            if (speed < 0f)
            speed += speedDifference * 1.5f;
            else
            speed += speedDifference;
        }
#if UNITY_STANDALONE_WIN
        if ((Input.GetKey(speedDown) || Input.GetKey(KeyCode.Joystick1Button6)) && !(speed <= speedMin))
#elif UNITY_PS4
        if ((Input.GetAxis("Joy1Axis9") > 0.1f) && !(speed <= speedMin))
#endif
        {
            if (speed > 0f)
                speed -= speedDifference * 1.5f;
            else
                speed -= speedDifference;
        }
        if(speed > 0f && speed < 1f && (!Input.GetKey(speedDown) && !Input.GetKey(speedUp) && Input.GetKey(KeyCode.Joystick1Button6)) && Input.GetKey(KeyCode.Joystick1Button7) && (Input.GetAxis("Joy1Axis8") > 0.1f) && (Input.GetAxis("Joy1Axis9") > 0.1f))
        {
            speed = 0f;
        }
        if (speed < 0f && speed > -1f && (!Input.GetKey(speedDown) && !Input.GetKey(speedUp) && Input.GetKey(KeyCode.Joystick1Button6)) && Input.GetKey(KeyCode.Joystick1Button7) && (Input.GetAxis("Joy1Axis8") > 0.1f) && (Input.GetAxis("Joy1Axis9") > 0.1f))
        {
            speed = 0f;
        }
        if (Input.GetKey(left))
        {
            if(speed >= 1f)
            {
                transform.RotateAround(transform.position, transform.forward, turnSpeed / 2 * Time.deltaTime);
                transform.RotateAround(transform.position, transform.up, -turnSpeed * Time.deltaTime);
            }
            else
                transform.RotateAround(transform.position, transform.up, -turnSpeed * Time.deltaTime);
        }
        if (Input.GetAxis("Joy1Axis0") < -0.1)
        {
            if (speed >= 1f)
            {
                transform.RotateAround(transform.position, transform.forward, turnSpeed * Time.deltaTime * -Input.GetAxis("Joy1Axis0"));
            }
            else
                transform.RotateAround(transform.position, transform.up, -turnSpeed * Time.deltaTime * -Input.GetAxis("Joy1Axis0"));
        }
        if (Input.GetKey(right))
        {
            if (speed >= 1f)
            {
                transform.RotateAround(transform.position, transform.forward, -turnSpeed / 2 * Time.deltaTime);
                transform.RotateAround(transform.position, transform.up, turnSpeed * Time.deltaTime);
            }
            else
                transform.RotateAround(transform.position, transform.up, turnSpeed * Time.deltaTime);
        }
        if(Input.GetAxis("Joy1Axis0") > 0.1)
        {
            if (speed >= 1f)
            {
                transform.RotateAround(transform.position, transform.forward, -turnSpeed * Time.deltaTime * Input.GetAxis("Joy1Axis0"));
            }
            else
                transform.RotateAround(transform.position, transform.up, turnSpeed * Time.deltaTime * Input.GetAxis("Joy1Axis0"));
        }
        if (Input.GetKey(up))
        {
            if(!inverted)
            transform.RotateAround(transform.position, transform.right, -turnSpeed * Time.deltaTime);
            else
                transform.RotateAround(transform.position, transform.right, turnSpeed * Time.deltaTime);
        }
        if(Input.GetAxis("Joy1Axis1") > 0.1)
        {
            if (!inverted)
                transform.RotateAround(transform.position, transform.right, -turnSpeed * Time.deltaTime * Input.GetAxis("Joy1Axis1"));
            else
                transform.RotateAround(transform.position, transform.right, turnSpeed * Time.deltaTime * Input.GetAxis("Joy1Axis1"));
        }
        if (Input.GetKey(down))
        {
            if(!inverted)
            transform.RotateAround(transform.position, transform.right, turnSpeed * Time.deltaTime);
            else
                transform.RotateAround(transform.position, transform.right, -turnSpeed * Time.deltaTime);
        }
        if(Input.GetAxis("Joy1Axis1") < -0.1)
        {
            if (!inverted)
                transform.RotateAround(transform.position, transform.right, turnSpeed * Time.deltaTime * -Input.GetAxis("Joy1Axis1"));
            else
                transform.RotateAround(transform.position, transform.right, -turnSpeed * Time.deltaTime * -Input.GetAxis("Joy1Axis1"));
        }
#if UNITY_STANDALONE_WIN
        if (Input.GetKey(rollLeft))
        {
            transform.RotateAround(transform.position, transform.forward, turnSpeed * Time.deltaTime);
        }
        if(Input.GetAxis("Joy1Axis2") > 0.1)
        {
            transform.RotateAround(transform.position, transform.forward, turnSpeed * Time.deltaTime * -Input.GetAxis("Joy1Axis2"));
        }
#elif UNITY_PS4
        if(Input.GetAxis("Joy1Axis3") > 0.1)
        {
            transform.RotateAround(transform.position, transform.forward, turnSpeed * Time.deltaTime * -Input.GetAxis("Joy1Axis3"));
        }
#endif
#if UNITY_STANDALONE_WIN
        if (Input.GetKey(rollRight))
        {
            transform.RotateAround(transform.position, transform.forward, -turnSpeed * Time.deltaTime);
        }
        if (Input.GetAxis("Joy1Axis2") < -0.1)
        {
            transform.RotateAround(transform.position, transform.forward, -turnSpeed * Time.deltaTime * Input.GetAxis("Joy1Axis2"));
        }
#elif UNITY_PS4
        if(Input.GetAxis("Joy1Axis3") < -0.1)
        {
            transform.RotateAround(transform.position, transform.forward, turnSpeed * Time.deltaTime * -Input.GetAxis("Joy1Axis3"));
        }
#endif
#if UNITY_STANDALONE_WIN
        if ((Input.GetKey(fire) || Input.GetKey(KeyCode.Joystick1Button1)) && ticker >= fireDelay)
#elif UNITY_PS4
        if (Input.GetKey(KeyCode.Joystick1Button0) && ticker >= fireDelay)
#endif
        {
            GameObject rb1 = Instantiate(projectile, transform.Find("FirePos1").position, transform.Find("FirePos1").rotation);
            rb1.GetComponent<Rigidbody>().AddForce(rb1.transform.forward * fireForce);
            GameObject rb2 = Instantiate(projectile, transform.Find("FirePos2").position, transform.Find("FirePos2").rotation);
            rb2.GetComponent<Rigidbody>().AddForce(rb2.transform.forward * fireForce);
            ticker = 0f;
            laser.Play();
        }

        if (ticker >= fireDelay && indicatorRenderer.material != fireYes)
        {
            indicatorRenderer.material = fireYes;
        }
        else if(indicatorRenderer.material != fireNo && !(ticker >= fireDelay))
        {
            indicatorRenderer.material = fireNo;
        }
        else
        {
            Debug.Log("Fuck");
        }
    }

    private void FixedUpdate()
    {
        Vector3 applyForce = Vector3.ClampMagnitude(transform.forward * speed, 50);
        rb.velocity = applyForce;
        sound.pitch = Mathf.Clamp(Mathf.Abs(transform.InverseTransformDirection(rb.velocity).z * 0.1f), minPitch, maxPitch);
    }
}

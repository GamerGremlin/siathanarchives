﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

[RequireComponent(typeof(Text))]
public class InputSniffer : MonoBehaviour {

    [Tooltip("Displays the active inputs at runtime")]
    [TextArea(1, 4)]
    public string ActiveInputs;

    [Tooltip("The deadzone to ignore on axes")]
    [Range(0, 1)]
    public float DeadZone = 0.1f;

    [Tooltip("Show the joystick name as registerd with the system when moving an axis")]
    public bool ShowJoystickName = false;

    protected int MaxJoysticks = 16;
    protected int MaxAxes = 28;

    protected Text text = null;
    protected string[] joystickNames;
    
    /////////
    // Use this for initialization
	void Awake()
    {
        text = GetComponent<Text>();
        Debug.Assert(text != null, "The InputSniffer must be attached to a UI.Text control");

        joystickNames = Input.GetJoystickNames();

        //CreateInputAssetsFileWithAllAvailableAxes();
    }
	
	// Update is called once per frame
	void Update ()
    {
        ActiveInputs = "";

        // Loop over all available buttons
        foreach (KeyCode k in Enum.GetValues(typeof(KeyCode)))
        {
            if (Input.GetKey(k))
            {
                ActiveInputs += k + "\n"; 
            }
        }

        // Loop over all Axes
        float axisValue;
        string axisName;
        for (int joy = 1; joy <= MaxJoysticks; joy++)
        {
            for (int axis = 0; axis <= MaxAxes; axis++)
            {
                axisName = GetAxisName(joy, axis);
                axisValue = Input.GetAxisRaw(axisName);

                if (Mathf.Abs(axisValue) > DeadZone)
                {
                    ActiveInputs += axisName + " [ " + axisValue + " ] ";
                    if (ShowJoystickName)
                        ActiveInputs += joystickNames[joy-1];
                    ActiveInputs += "\n";
                }
            }
        }


        // Display on UI text, if available
        if (text != null)
            text.text = ActiveInputs;
	}

    string GetAxisName(int joy, int axis)
    {
        return "Joy" + joy + "Axis" + axis;
    }

    // Helper function to write a new InputManager.asset with all the Joystick Axes setup
    // Reads the original and writes a new file.
    // Simply replace the original file to add the new axes
    void CreateInputAssetsFileWithAllAvailableAxes()
    {
        StreamReader reader = new StreamReader("ProjectSettings/InputManager.asset");
        string inputManagerAsset = reader.ReadToEnd();
        reader.Close();

        // We
        StreamWriter writer = new StreamWriter("ProjectSettings/InputManager.asset.new");
        writer.Write(inputManagerAsset);

        // Now write all the new joystick axes
        for (int joy = 1; joy <= MaxJoysticks; joy++)
        {
            for(int axis = 0; axis <= MaxAxes; axis++)
            {
                writer.WriteLine("  - serializedVersion: 3");
                writer.WriteLine("    m_Name: " + GetAxisName(joy, axis));
                writer.WriteLine("    descriptiveName:");
                writer.WriteLine("    descriptiveNegativeName:");
                writer.WriteLine("    negativeButton:");
                writer.WriteLine("    positiveButton:");
                writer.WriteLine("    altNegativeButton:");
                writer.WriteLine("    altPositiveButton:");
                writer.WriteLine("    gravity: 3");
                writer.WriteLine("    dead: 0.001");
                writer.WriteLine("    sensitivity: 3");
                writer.WriteLine("    snap: 0");
                writer.WriteLine("    invert: 0");
                writer.WriteLine("    type: 2");
                writer.WriteLine("    axis: " + axis);
                writer.WriteLine("    joyNum: " + joy);
            }
        }

        writer.Close();
    }
}

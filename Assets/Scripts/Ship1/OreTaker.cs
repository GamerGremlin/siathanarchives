﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OreTaker : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        OreGatherer script = other.GetComponent<OreGatherer>();
        if(script != null)
        {
            script.holding = 0;
            script.holdingText.text = 0.ToString();
        }
    }
}

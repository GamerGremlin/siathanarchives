﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

    public GameObject[] toSpawn;
    public Transform posMin;
    public Transform posMax;
    public int numberToSpawn;
    [Range(1, 10)]
    public int maxScale;

    private void Awake()
    {
        for(int i = 0; i < numberToSpawn; i++)
        {
            GameObject obj = Instantiate(toSpawn[Random.Range(0, toSpawn.Length)], new Vector3(Random.Range(posMin.position.x, posMax.position.x), Random.Range(posMin.position.y, posMax.position.y), Random.Range(posMin.position.z, posMax.position.z)), Random.rotation);
            float scale = Random.Range(0f, (float)maxScale);
            obj.transform.localScale = new Vector3(scale, scale, scale);
        }
    }
}

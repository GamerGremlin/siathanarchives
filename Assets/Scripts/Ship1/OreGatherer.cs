﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OreGatherer : MonoBehaviour {

    public int holdLimit;
    [System.NonSerialized]
    public int holding;
    public Text limitText;
    public Text holdingText;

    private void Start()
    {
        holding = 0;
        limitText.text = holdLimit.ToString();
        holdingText.text = holding.ToString();
    }

    // Update is called once per frame
    void Update ()
    {
	}

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Ore" && !(holding >= holdLimit))
        {
            Destroy(other.gameObject);
            holding++;
            holdingText.text = holding.ToString();
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CaithlynText : MonoBehaviour
{
    public Text textBox;
    [System.Serializable]
    public class TextDisplay
    {
        public float letterDelay;
        public string text;
        public float startDelay;
        [System.NonSerialized]
        public bool triggered;
    }
    public TextDisplay[] textBoxes;

    private float ticker;
    void Start()
    {
        ticker = 0f;
    }

    void Update()
    {
        if (!(ticker >= textBoxes[textBoxes.Length - 1].startDelay))
        {
            ticker += Time.deltaTime;
        }
        for(int i = 0; i < textBoxes.Length; i++)
        {
            if(ticker >= textBoxes[i].startDelay && !textBoxes[i].triggered)
            {
                StartCoroutine(AnimateText(textBoxes[i].text, i));
                textBoxes[i].triggered = true;
            }
        }
    }

    IEnumerator AnimateText(string strComplete, int index)
    {
        int i = 0;
        string str = "";
        while (i < strComplete.Length)
        {
            str += strComplete[i++];
            yield return new WaitForSeconds(textBoxes[index].letterDelay);
            textBox.text = str;
        }
    }
}

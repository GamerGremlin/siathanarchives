﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrbRotate : MonoBehaviour {

    public GameObject lookAtTarget;

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        transform.rotation = Quaternion.LookRotation(lookAtTarget.transform.position-transform.position);
	}
}

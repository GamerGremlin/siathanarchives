﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroid : MonoBehaviour {

    public int maxHealth;
    private int currentHealth;
    public int damageTake;
    public GameObject[] spawnOnDeath;
    [Range(1, 10)]
    public int amountToSpawn;


    // Use this for initialization
    void Start()
    {
        currentHealth = maxHealth;
        if (spawnOnDeath == null)
        {
            Debug.Log("No object will be spawned on death, ensure this is desired");
        }
    }

    public void CheckDeath()
    {
        currentHealth -= damageTake;
        if(currentHealth <= 0)
        {
            for(int i = 0; i < amountToSpawn; i++)
            {
                GameObject obj = Instantiate(spawnOnDeath[Random.Range(0, spawnOnDeath.Length)], new Vector3((gameObject.transform.position.x + Random.Range(-5, 5)), (gameObject.transform.position.y + Random.Range(-5, 5)), (gameObject.transform.position.z + Random.Range(-5, 5))), Random.rotation);
                obj.GetComponent<Rigidbody>().AddForce(new Vector3(Random.Range(0.1f, 1f), Random.Range(0.1f, 1f), Random.Range(0.1f, 1f)), ForceMode.Impulse);
            }
            Destroy(gameObject);
        }
    }
}

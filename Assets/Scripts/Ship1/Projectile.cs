﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

    public float destroyAt = 5f;

    private float ticker;

	// Use this for initialization
	void Start ()
    {
        ticker = 0f;
	}
	
	// Update is called once per frame
	void Update ()
    {
        ticker += Time.deltaTime;

        if(ticker >= destroyAt)
        {
            Destroy(gameObject);
        }
	}

    private void OnTriggerEnter(Collider other)
    {
        if(other.GetComponent<Asteroid>())
        {
            other.GetComponent<Asteroid>().CheckDeath();
            Destroy(gameObject);
        }
    }
}

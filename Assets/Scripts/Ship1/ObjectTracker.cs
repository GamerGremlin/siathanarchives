﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectTracker : MonoBehaviour {


    public string objectTag;
    public GameObject[] objects;

	// Use this for initialization
	void Start ()
    {
        objects = GameObject.FindGameObjectsWithTag(objectTag);
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}
}

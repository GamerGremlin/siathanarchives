﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyDelay : MonoBehaviour {


    private float ticker;
    public float delay;
    public GameObject item;
	// Use this for initialization
	void Awake ()
    {
        ticker = 0f;
        if(item == null)
        {
            item = gameObject;
        }
	}
	
	// Update is called once per frame
	void Update ()
    {
		if(ticker >= delay)
        {
            Destroy(item);
        }
        else
        {
            ticker += Time.deltaTime;
        }
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using cakeslice;

public class MouseOverHighlight : MonoBehaviour {

    private Outline outlineScript;

	// Use this for initialization
	void Start ()
    {
        outlineScript = GetComponent<Outline>();
        outlineScript.enabled = false;
    }
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    private void OnMouseEnter()
    {
        outlineScript.enabled = true;
    }

    private void OnMouseExit()
    {
        outlineScript.enabled = false;
    }
}

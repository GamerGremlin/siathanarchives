﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(Collider))]
[DisallowMultipleComponent]
public class PushObject : MonoBehaviour {

    [Tooltip("How far the object will be pushed each time the object is in the collider")]
    public float pushSpeed;
    public KeyCode pullButton = KeyCode.F;

    private void Awake()
    {
       if(GetComponent<Collider>().isTrigger == false)
        {
            Debug.Log("Please ensure that the collider attached to this object is set to a trigger");
        }
    }

    // Use this for initialization
    void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerStay(Collider other)
    {
        if(other.gameObject.GetComponent<Pushable>() != null)
        {
            Pushable pushing = other.gameObject.GetComponent<Pushable>();
            if (Input.GetKey(pullButton))
                pushing.Push(pushSpeed, -transform.forward);
            else
                pushing.Push(pushSpeed, transform.forward);
        }
    }
}

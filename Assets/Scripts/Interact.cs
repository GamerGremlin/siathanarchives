﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interact : MonoBehaviour {

    public Vector3 size;
    public float range;
    public KeyCode interact;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		if(Input.GetKeyDown(interact))
        {
            RaycastHit[] hits = Physics.BoxCastAll(transform.position, size, transform.forward, transform.rotation, range);
            foreach(RaycastHit hit in hits)
            {
                if(hit.transform.GetComponent<Interactable>())
                {
                    hit.transform.GetComponent<Interactable>().Interact(this);
                }
            }
        }
	}
}

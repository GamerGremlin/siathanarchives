﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(Cloth))]
public class ForceOnCloth : MonoBehaviour {

    private Cloth clothyBoy;

    public Vector3 minimumForce = Vector3.one;
    public Vector3 maximumForce = Vector3.one;

	void Awake ()
    {
		clothyBoy = GetComponent<Cloth>();
    }

	void Update ()
    {
        clothyBoy.externalAcceleration = new Vector3(Random.Range(minimumForce.x, maximumForce.x), Random.Range(minimumForce.y, maximumForce.y), Random.Range(minimumForce.z, maximumForce.z));
	}
}

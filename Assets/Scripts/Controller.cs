﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;

public class Controller : MonoBehaviour {

    public float rotSpeed;
    private NavMeshAgent agent;
    public GameObject particleBlock;
    private GameObject loc;
    private int pathCount = 0;
    public float cornerSlow;
    public float cornerSlowLimit;
    // Use this for initialization
    void Start ()
    {
        agent = GetComponent<NavMeshAgent>();
        loc = null;
		agent.updateRotation = false;
	}

    // Update is called once per frame
    void Update()
    {

        if (agent.remainingDistance <= 1 && loc != null)
        {
            Destroy(loc);
            loc = null;
        }

        for (int i = 0; i < agent.path.corners.Length - 1; i++)
        {
            Debug.DrawLine(agent.path.corners[i], agent.path.corners[i + 1], Color.red);
        }



        if (Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject())
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, Mathf.Infinity))
            {
                if (hit.transform.gameObject.tag == "Ground")
                {
                    if (loc == null)
                    {
                        loc = Instantiate(particleBlock, hit.point, hit.transform.rotation);
                    }
                    else
                    {
                        Destroy(loc);
                        loc = Instantiate(particleBlock, hit.point, hit.transform.rotation);
                    }
                    agent.destination = hit.point;
                    pathCount = agent.path.corners.Length;
                }
            }
        }
        InstantlyTurn (agent.destination);
    }

	private void InstantlyTurn(Vector3 destinaton)
	{
        if (agent.path.corners.Length > 1)
        {
            if (pathCount != agent.path.corners.Length)
            {
                if(Vector3.Angle((agent.path.corners[1] - transform.position), transform.forward) > cornerSlowLimit)
                {
                    Debug.Log("Reached corner...");
                    agent.velocity = agent.velocity * cornerSlow;
                    pathCount = agent.path.corners.Length;
                }
            }
            Vector3 direction = (agent.path.corners[1] - transform.position);
            direction.y = 0;
            Quaternion qDir = Quaternion.LookRotation(direction);
            transform.rotation = Quaternion.Slerp(transform.rotation, qDir, Time.deltaTime * rotSpeed);
        }
	}
}

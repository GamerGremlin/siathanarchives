﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneTransition : MonoBehaviour {

    public string changeScene;

	// Use this for initialization
	void Start ()
    {
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public void SetSceneChange(string changeTo)
    {
        changeScene = changeTo;
    }

    public void ChangeScene()
    {
        SceneManager.LoadScene(changeScene, LoadSceneMode.Single);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Used to ensure that the CharacterController component is added
[RequireComponent(typeof(CharacterController))]
//Ensuring more than one of this script cannnot be attached
[DisallowMultipleComponent]
public class CharMove : MonoBehaviour
{

    public enum CameraMovementTypes
    {
        FreeOrbit,
        LockedBehind
    }

    //Private variables to pointless GetComponents
    //GetComponent is an expensive operation. Use GetComponent once and cache it if possible.
    private CharacterController charControl;
    private Transform ownTransform;
    private Camera camComponent;

    //Stored numbers to avoid grabbing them each time
    private float baseSpeed;
    private float verticalForce;
    private bool crosshair;
    private float mainFOV;
    private bool toggledView;
    private bool moveLocked;

    //Header is used to put labels above variables
    [Header("Movement Settings")]

    //Tooltips are shown on hovering over the variable
    [Tooltip("Movement Speed")]
    public float speed;
    [Tooltip("Jump Height")]
    public float jumpHeight;
    [Tooltip("The force applied after the jump")]
    public float gravity;
    [Tooltip("Maximum Velocity")]
    public float maxVerticalForce = 10;
    [Tooltip("Rotation Speed")]
    public float rotSpeed;
    [Tooltip("Speed Multiplier for sprinting")]
    public float sprintMulti;
    [Tooltip("Leeway for when the model is considered 'grounded'")]
    public float groundedTolerance;

    [Header("Camera Settings")]

    [Tooltip("Camera to use")]
    public GameObject cam;
    [Tooltip("Positions for the camera to transition to. Element 0 must always be the original position")]
    public Transform[] cameraPositions;
    [Tooltip("Speed for the camera to slide to the different position")]
    public float slideTime;
    [Tooltip("Canvas object for a targeting crosshair when alternative view is active. Leave empty if undesired.")]
    public GameObject rectical;
    [Tooltip("Is the zoom a hold down or toggle? Ticked is hold down.")]
    public bool zoomFunction = true;
    [Tooltip("How will the camera move? If Free Orbit is chosen, ensure the player model is a child of the player center")]
    public CameraMovementTypes cameraMovement = CameraMovementTypes.LockedBehind;
    [Tooltip("Is the player movement locked during zoom?")]
    public bool zoomLock = false;

    [Tooltip("Maximum downwards angle that the camera can tilt to. 90 is straight down.")]
    //Range is put a slider between the two variables. Note that this does NOT clamp the variable, a user is able to input the number manually to override.
    [Range(0, 90)]
    public float ClampAngleLower = 90;
    [Tooltip("Maximum upwards angle that the camera can tilt to. 90 is straight up.")]
    [Range(0, 90)]
    public float ClampAngleUpper = 90;
    [Tooltip("Field of View for secondary perspective")]
    [Range(1, 179)]
    public float fovAdjust = 60;

    [Header("Controls")]
    public KeyCode moveLeft = KeyCode.A;
    public KeyCode moveRight = KeyCode.D;
    public KeyCode moveForward = KeyCode.W;
    public KeyCode moveBackward = KeyCode.S;
    public KeyCode jump = KeyCode.Space;
    [Tooltip("Set to None for no sprint")]
    public KeyCode sprint = KeyCode.LeftShift;
    [Tooltip("Used to adjust between main view and alternate view. Remember that Mouse0 is left click.")]
    public KeyCode zoom = KeyCode.Mouse1;

    void Start()
    {
        //Crosshair is set to true if rectical is not null.
        crosshair = rectical != null;

        //Set up private variables
        charControl = GetComponent<CharacterController>();
        ownTransform = transform;
        baseSpeed = speed;
        camComponent = cam.GetComponent<Camera>();
        mainFOV = camComponent.fieldOfView;
        toggledView = false;
    }

    // Update is called once per frame
    void Update()
    {

        //Multiply speed by multiplier when sprint button held down.

        //Why not have speed equal speed times multiplier? This would result in endless multiplication of speed.
        if (Input.GetKey(sprint))
        {
            speed = baseSpeed * sprintMulti;
        }
        else
        {
            speed = baseSpeed;
        }

        //Basic movements
        if (!moveLocked)
        {
            if (Input.GetKey(moveForward))
            {
                charControl.Move(transform.forward * speed * Time.deltaTime);
            }
            if (Input.GetKey(moveBackward))
            {
                charControl.Move(-transform.forward * speed * Time.deltaTime);
            }
            if (Input.GetKey(moveLeft))
            {
                charControl.Move(-transform.right * speed * Time.deltaTime);
            }
            if (Input.GetKey(moveRight))
            {
                charControl.Move(transform.right * speed * Time.deltaTime);
            }
        }

        //Apply gravity force
        verticalForce -= (gravity * Time.deltaTime);
        verticalForce = Mathf.Clamp(verticalForce, -maxVerticalForce, maxVerticalForce);


        //Apply jump if grounded
        if (Input.GetKeyDown(jump) && IsGrounded())
        {
            verticalForce = jumpHeight;
        }

        //Adjust view if alternate view control is held down.
        //Could use some cleaning but it's functional
        if (zoomFunction)
        {
            if (Input.GetKey(zoom))
            {
                cam.transform.position = Vector3.Slerp(cam.transform.position, cameraPositions[1].transform.position, slideTime);
                //cam.transform.rotation = Quaternion.Slerp(cam.transform.rotation, cameraPositions[1].transform.rotation, slideTime);
                Cursor.lockState = CursorLockMode.None;
                if (crosshair)
                {
                    rectical.SetActive(true);
                }
                if (zoomLock)
                {
                    moveLocked = true;
                }
                camComponent.fieldOfView = fovAdjust;
            }
            else
            {
                cam.transform.position = Vector3.Slerp(cam.transform.position, cameraPositions[0].transform.position, slideTime);
                //cam.transform.rotation = Quaternion.Slerp(cam.transform.rotation, cameraPositions[0].transform.rotation, slideTime);
                Cursor.lockState = CursorLockMode.Locked;
                if (crosshair)
                {
                    rectical.SetActive(false);
                }
                if (zoomLock)
                {
                    moveLocked = false;
                }
                camComponent.fieldOfView = mainFOV;
            }
        }
        else
        {
            //Difference between GetKey and GetKeyDown: GetKey will fire for each frame the key is held down. GetKeyDown will only fire once when the key is pressed down, it must be released for it to fire again on another press.
            if (Input.GetKeyDown(zoom))
            {
                //Confusing, but this will inverse toggledView, so if it was true it becomes false, false becomes true.
                toggledView = !toggledView;
            }
            if (toggledView)
            {
                cam.transform.position = Vector3.Slerp(cam.transform.position, cameraPositions[1].transform.position, slideTime);
                //cam.transform.rotation = Quaternion.Slerp(cam.transform.rotation, cameraPositions[1].transform.rotation, slideTime);
                Cursor.lockState = CursorLockMode.None;
                if (crosshair)
                {
                    rectical.SetActive(true);
                }
                if (crosshair)
                {
                    rectical.SetActive(true);
                }
                if (zoomLock)
                {
                    moveLocked = true;
                }
                camComponent.fieldOfView = fovAdjust;
            }
            else
            {
                cam.transform.position = Vector3.Slerp(cam.transform.position, cameraPositions[0].transform.position, slideTime);
                //cam.transform.rotation = Quaternion.Slerp(cam.transform.rotation, cameraPositions[0].transform.rotation, slideTime);
                if (crosshair)
                {
                    rectical.SetActive(false);
                }
                if (crosshair)
                {
                    rectical.SetActive(false);
                }
                if (zoomLock)
                {
                    moveLocked = false;
                }
                camComponent.fieldOfView = mainFOV;
            }
        }

        //Tilt view based on mouse up and down movement
        charControl.Move(transform.up * verticalForce * Time.deltaTime);
        float camTilt = cam.transform.localEulerAngles.x + -Input.GetAxis("Mouse Y");


        //Important: DO NOT TOUCH ANY OF THIS

        //Note: Even I struggle to understand this

        //Clamp rotation between the set variables
        if ((camTilt > ClampAngleLower) && (camTilt < 180))
            camTilt = ClampAngleLower;
        else if ((camTilt < (360 - ClampAngleUpper)) && (camTilt > 180))
            camTilt = (360 - ClampAngleUpper);

        //Change the camera rotation to the clamped rotation
        cam.transform.localEulerAngles = new Vector3(camTilt, 0, 0);

        //End do not touch

        if (cameraMovement == CameraMovementTypes.LockedBehind)
        {
            //Character rotation according to left and right mouse input
            float rot = 0;
            rot = rotSpeed * Input.GetAxis("Mouse X");
            ownTransform.Rotate(0, rot, 0);
        }
    }

    public bool IsGrounded()
    {
        //Check if the player is grounded, taking into account the tolerance
        return Physics.Raycast(transform.position, -ownTransform.up, groundedTolerance);
    }

    public void ToggleLocked()
    {
        moveLocked = !moveLocked;
    }
}

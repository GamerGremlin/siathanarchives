﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
public class CameraController : MonoBehaviour {

    [Tooltip("Transform locations. Requires at least two")]
public Transform[] locations;
    [Tooltip("Key to change camera location")]
public KeyCode nextView;

private int currentView;
    public GameObject cam;
    // Use this for initialization
    void Start () 
	{
		currentView = 0;
        if(cam == null)
        cam = Camera.main.gameObject;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(Input.GetKeyDown(nextView))
		{
			currentView++;
			if(currentView == locations.Length)
			{
				currentView = 0;
			}
			cam.transform.position = locations[currentView].position;
			cam.transform.rotation = locations[currentView].rotation;
			
		}
	}
}

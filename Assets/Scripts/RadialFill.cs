﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RadialFill : MonoBehaviour {

    public Image target;
    [Range(0, 1)]
    public float fillAmount;
    [Range(0, 1)]
    public float maxFill;
    [Range(0, 1)]
    public float minFill;

	// Update is called once per frame
	void Update ()
    {
		if(fillAmount > 1f)
        {
            fillAmount = fillAmount / 100f;
        }

        

        target.fillAmount = Mathf.Clamp(fillAmount, minFill, maxFill);
    }
}

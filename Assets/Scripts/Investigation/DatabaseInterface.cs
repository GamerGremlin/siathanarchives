﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class DatabaseInterface : MonoBehaviour {

    public Database db;

    public Transform displayField;

    private int index;
    private int lastIndex;

    private Image img;
    private Text desc;
    private Text itemName;

    private void Awake()
    {
        if(db != null)
        db = GetComponent<Database>();

        index = 0;
        lastIndex = 0;

        img = transform.Find("Icon").GetComponent<Image>();
        desc = transform.Find("Description").GetComponent<Text>();
        itemName = transform.Find("Name").GetComponent<Text>();
    }

    void Update ()
    {
        if (Input.GetKeyDown(KeyCode.DownArrow) && !(index >= db.GetDBSize()))
        {
            index++;
        }
        if(Input.GetKeyDown(KeyCode.UpArrow) && !(index <= 0))
        {
            index--;
        }
        if(lastIndex != index)
        {
            DatabaseEntry temp = db.AccessDBItem(index);
            lastIndex = index;
            img.sprite = temp.icon;
            desc.text = temp.description;
            itemName.text = temp.name;
        }
	}

    void DatabaseUpdated()
    {

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DatabaseEntries : MonoBehaviour
{
    public Database db;


    private void Awake()
    {
        UpdateFields();
    }

    void UpdateFields()
    {
        for(int i = 0; i < transform.childCount; i++)
        {
            DatabaseEntry temp = db.AccessDBItem(i);

            transform.GetChild(i).GetComponent<Image>().sprite = temp.icon;
            transform.GetChild(i).GetComponent<Text>().text = temp.name;
        }
    }
}

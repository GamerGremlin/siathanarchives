﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(Collider))]
public class DatabaseItem : MonoBehaviour
{
    public Sprite icon;
    public string entryName;
    public string description;
    public int index;
}

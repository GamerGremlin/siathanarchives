﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class DatabaseEntry
{
    public Sprite icon;
    public string name;
    public string description;

    public DatabaseEntry(Sprite sprite, string entryName, string desc)
    {
        icon = sprite;
        name = entryName;
        description = desc;
    }

    public DatabaseEntry()
    {
        icon = null;
        name = "Blank";
        description = "Blank";
    }
}

public class Database : MonoBehaviour {


    public Sprite defaultIcon;
    public string defaultName;
    public string defaultDesc;

    public Transform insertPoint;

    List<DatabaseEntry> db;

	void Start ()
    {
        db = new List<DatabaseEntry>();

        DatabaseItem[] array = FindObjectsOfType<DatabaseItem>();

        for(int i = 0; i < array.Length; i++)
        {
            db.Add(new DatabaseEntry(defaultIcon, defaultName, defaultDesc));
        }
        for(int i = insertPoint.childCount; i > array.Length - 1; i++)
        {
            Instantiate(insertPoint.GetChild(0), insertPoint);
        }
	}

    public int GetDBSize()
    {
        return db.Count;
    }

    public DatabaseEntry AccessDBItem(int index)
    {
        return db[index];
    }

    public void AddEntry(DatabaseItem entry)
    {
        if (db[entry.index].name != defaultName)
            return;

        db.RemoveAt(entry.index);

        DatabaseEntry toInsert = new DatabaseEntry(entry.icon, entry.entryName, entry.description);

        db.Insert(entry.index, toInsert);
    }
}

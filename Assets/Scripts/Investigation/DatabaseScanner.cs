﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DatabaseScanner : MonoBehaviour {

    public Transform rayPoint;
    public Database db;

    private void Update()
    {
        RaycastHit hit;
        Physics.Raycast(rayPoint.position, rayPoint.forward, out hit);
        DatabaseItem temp;
        if (hit.transform != null)
        {
            temp = hit.transform.GetComponent<DatabaseItem>();
        }
        else
        {
            return;
        }
        if (temp != null)
        {
            db.AddEntry(temp);
        }
    }
}

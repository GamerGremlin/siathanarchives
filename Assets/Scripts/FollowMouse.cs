﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowMouse : MonoBehaviour {

    public float speed;
    private RectTransform trans;

	// Use this for initialization
	void Start ()
    {
        trans = gameObject.GetComponent<RectTransform>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        //trans.position = Vector3.Lerp(trans.position, new Vector3(Screen.width / 2, Input.mousePosition.y, 0), speed);
        trans.position = Vector3.Lerp(trans.position, Input.mousePosition, speed);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableDisableInteract : Interactable {

    public GameObject[] toEnable;
    public GameObject[] toDisable;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public override void Interact(Interact sender)
    {
        if (toEnable.Length != 0)
        {
            for (int i = 0; i < toEnable.Length; i++)
            {
                toEnable[i].SetActive(true);
            }
        }
        if (toDisable.Length != 0)
        {
            for (int i = 0; i < toDisable.Length; i++)
            {
                toDisable[i].SetActive(false);
            }
        }
    }
}

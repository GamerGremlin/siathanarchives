﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableOnDelay : MonoBehaviour {

	public float delay;
	private float ticker;
	private bool isTicking;
	public GameObject[] toEnable;

	// Use this for initialization
	void Start () 
	{
		for (int i = 0; i < toEnable.Length; i++) 
		{
			toEnable[i].SetActive(false);
		}
		isTicking = true;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (isTicking)
			ticker += Time.deltaTime;

		if (ticker >= delay) 
		{
			isTicking = false;
			for (int i = 0; i < toEnable.Length; i++) 
			{
				toEnable[i].SetActive(true);
			}
		}
	}


}

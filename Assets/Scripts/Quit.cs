﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Quit : MonoBehaviour {

    public KeyCode exitButton;

	
	// Update is called once per frame
	void Update ()
    {
		if(Input.GetKey(exitButton) || Input.GetKey(KeyCode.Joystick1Button12))
        {
            Application.Quit();
        }
	}
}

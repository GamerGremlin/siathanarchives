﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeInOut : MonoBehaviour {

    public enum FadeType
    {
        IN,
        OUT
    }

    public FadeType fadeType;
    public float fadeSpeed;
    public float delay;
    private Image image;
    private Text text;
    private float ticker;
	// Use this for initialization
	void Awake ()
    {
        image = GetComponent<Image>();
        if(image == null)
        {
            text = GetComponent<Text>();
        }
        if(image == null && text == null)
        {
            Debug.Log("No object to fade could be found", this);
        }
        ticker = 0f;
	}
	
	// Update is called once per frame
	void Update ()
    {
        if(ticker < delay)
        {
            ticker += Time.deltaTime;
        }
        if (ticker >= delay)
        {
            if (fadeType == FadeType.IN)
            {
                if (image != null && image.color.a >= 255f)
                {
                    image.color = new Color(image.color.r, image.color.g, image.color.b, image.color.a + fadeSpeed * Time.deltaTime);
                }
                else if (text != null)
                {
                    text.color = new Color(text.color.r, text.color.g, text.color.b, text.color.a + fadeSpeed * Time.deltaTime);
                }
                else
                {
                    Debug.Log("No object to fade in", this);
                }
            }
            else if (fadeType == FadeType.OUT)
            {
                if (image != null)
                {
                    image.color = new Color(image.color.r, image.color.g, image.color.b, image.color.a - fadeSpeed * Time.deltaTime);
                }
                else if (text != null)
                {
                    text.color = new Color(text.color.r, text.color.g, text.color.b, text.color.a - fadeSpeed * Time.deltaTime);
                }
                else
                {
                    Debug.Log("No object to fade in", this);
                }
            }
        }
    }
}

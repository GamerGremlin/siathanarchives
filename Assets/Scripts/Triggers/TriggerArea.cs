﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerArea : MonoBehaviour {

    [Tooltip("Object(s) to trigger")]
    public Triggerable[] toTrigger;
    [Tooltip("Will this only trigger once?")]
    public bool triggerOnce;

    private bool triggered = false;

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player" && !triggered)
        {
            foreach(Triggerable triggerObject in toTrigger)
            {
                triggerObject.TriggerObject();
            }
            if(triggerOnce)
            {
                triggered = true;
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerableGameObject : Triggerable {

    public GameObject[] objects;
    [Tooltip("Should the object(s) be enabled or disabled?")]
    public bool enable;

    public override void TriggerObject()
    {
        foreach (GameObject item in objects)
        {
            item.SetActive(enable);
        }
    }
}

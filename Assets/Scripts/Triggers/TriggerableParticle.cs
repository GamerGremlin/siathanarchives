﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerableParticle : Triggerable {

    public ParticleSystem[] particles;

    public override void TriggerObject()
    {
        foreach (ParticleSystem particleObjcets in particles)
        {
            particleObjcets.Play();
        }
    }
}

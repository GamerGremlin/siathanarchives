﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerableSound : Triggerable {

    public AudioClip soundFile;
    public AudioSource sourceObject;

    public override void TriggerObject()
    {
        sourceObject.clip = soundFile;
        sourceObject.Play();
    }
}

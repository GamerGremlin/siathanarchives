﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextFlash : MonoBehaviour {

    public Text textBox;
    public string[] flashList;
    public int delay;
    private int ticker;
    private int currentFrame;

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        ticker++;
        
        if(ticker >= delay)
        {
            ticker = 0;
            currentFrame++;
            if(currentFrame >= flashList.Length)
            {
                currentFrame = 0;
            }
            textBox.text = flashList[currentFrame];
        }


	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldFlicker : MonoBehaviour {

    private Material mRenderer;
    private byte alpha;
    public byte alphaMin;
    public byte alphaMax;
    private float ticker;
    public float shiftDelay;
    private Color32 col;
    private bool tickUp;
    private bool disableShield;
    private float disableTicker;
    public float disableAt;

	// Use this for initialization
	void Start ()
    {
        mRenderer = GetComponent<MeshRenderer>().material;
        col = mRenderer.color;
        tickUp = true;
        disableShield = false;
	}
	
	// Update is called once per frame
	void Update ()
    {
        ticker += Time.deltaTime;

        if (!disableShield)
        {
            disableTicker += Time.deltaTime;
        }

        if(disableTicker >= disableAt)
        {
            disableShield = true;
        }

        if (!disableShield)
        {
            if (alpha > alphaMax)
            {
                tickUp = false;
            }
            if (alpha < alphaMin)
            {
                tickUp = true;
            }
        }

        if (ticker >= shiftDelay && !disableShield)
        {
            ticker = 0;
            if(tickUp)
            {
                alpha++;
            }
            else
            {
                alpha--;
            }
            col.a = alpha;
            mRenderer.SetColor("_Color", col);
        }

        if(disableShield && ticker >= shiftDelay)
        {
            ticker = 0f;
            if (alpha > 0)
            {
                alpha--;
            }
            col.a = alpha;
            mRenderer.SetColor("_Color", col);
        }
	}
}

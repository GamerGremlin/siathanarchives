﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonPress : MonoBehaviour {

	public GameObject toPress;
	public CallAnim animScript;
	public string animToCall;
    public SceneTransition sceneChanger;
    private int ticker;
    private bool isTicking;
    public int delay;

	// Use this for initialization
	void Start ()
    {
        ticker = 0;
        isTicking = false;
	}
	
	// Update is called once per frame
	void Update () 
	{
        if(isTicking)
        {
            ticker++;
        }

        if(ticker > delay)
        {
            sceneChanger.ChangeScene();
        }

		if(Input.GetMouseButtonDown(0))
			{
			RaycastHit hit;
			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			if (Physics.Raycast (ray, out hit)) 
			{
				if (hit.collider.gameObject == toPress) 
				{
                    animScript.CallAnimation(animToCall);
                    isTicking = true;
				}
			}
			}
	}
}

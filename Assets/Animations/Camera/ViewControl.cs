﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CurrentState
{
    left,
    right,
    center
}

public class ViewControl : MonoBehaviour {

    public Animator theObject;
    public KeyCode leftKey;
    public KeyCode rightKey;
    private CurrentState state;
    private int ticker;
    public int delay;
    private bool isTicking;
    public int startDelay;

    // Use this for initialization
    void Start ()
    {
        ticker = -startDelay;
        state = CurrentState.center;
        if(theObject == null)
        {
            theObject = GetComponent<Animator>();
        }
        isTicking = true;
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (isTicking)
        {
            ticker++;
            if(ticker >= delay)
            {
                isTicking = false;
            }
        }

		if(Input.GetKeyDown(leftKey) && ticker >= delay)
        {
            if(state == CurrentState.center)
            {
                theObject.Play("RotateLeft");
                state = CurrentState.left;
                ticker = 0;
                isTicking = true;
            }
            else if(state == CurrentState.right)
            {
                theObject.Play("ReturnFromRight");
                state = CurrentState.center;
                ticker = 0;
                isTicking = true;
            }
        }

        if(Input.GetKeyDown(rightKey) && ticker >= delay)
        {
            if (state == CurrentState.center)
            {
                theObject.Play("RotateRight");
                state = CurrentState.right;
                ticker = 0;
                isTicking = true;
            }
            else if(state == CurrentState.left)
            {
                theObject.Play("ReturnFromLeft");
                state = CurrentState.center;
                ticker = 0;
                isTicking = true;
            }
        }
	}
}

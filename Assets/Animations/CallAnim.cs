﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CallAnim : MonoBehaviour {

	public Animator item;

	// Use this for initialization
	void Start () 
	{
		if (item == null) 
		{
			item = GetComponent<Animator> ();
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void CallAnimation(string anim)
	{
		item.Play (anim);
	}
}

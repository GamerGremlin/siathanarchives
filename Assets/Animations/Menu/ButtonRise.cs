﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ButtonStates
{
    LOWERED,
    RISEN
}

public class ButtonRise : MonoBehaviour {

    private ButtonStates state;
    private Animator animController;

	// Use this for initialization
	void Start ()
    {
        state = ButtonStates.LOWERED;
        animController = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public void PlayAnim()
    {
        if(state == ButtonStates.LOWERED)
        {
            animController.Play("ComeUp");
            state = ButtonStates.RISEN;
        }
    }
}

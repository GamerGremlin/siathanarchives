﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum states
{
	UP,
	DOWN
}

public class ArrowMove : MonoBehaviour {


	private Animator animController;
	private int ticker;
	private bool isTicking;
	public int delay;
	[System.NonSerialized]
	public states currentState;
	// Use this for initialization
	void Start () 
	{
		currentState = states.UP;
		animController = GetComponent<Animator>();
		isTicking = true;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (isTicking)
			ticker++;

		if (ticker > delay)
			isTicking = false;
	}

	public void MoveUp()
	{
		if (currentState != states.UP && ticker > delay) 
		{
			animController.Play ("ArrowUp");
			currentState = states.UP;
			ticker = 0;
			isTicking = true;
		}
	}

	public void MoveDown()
	{
		if (currentState != states.DOWN && ticker > delay) 
		{
			animController.Play ("ArrowDown");
			currentState = states.DOWN;
			ticker = 0;
			isTicking = true;
		}
	}
}

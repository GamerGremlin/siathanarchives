﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AnimPack : System.Object
{
    public Animator item;
    public float timer;
    public string animName;
    [System.NonSerialized]
    public bool triggered = false;
}

public class AnimationTimer : MonoBehaviour {

    private float ticker;
    private bool isTicking;

    public AnimPack[] animList;

    // Use this for initialization
    void Start ()
    {
        ticker = 0;
        isTicking = true;
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (isTicking)
        {
            ticker += Time.deltaTime;
        }

		if (ticker > animList[animList.Length - 1].timer) 
		{
			isTicking = false;
		}

        for(int i = 0; i < animList.Length; i++)
        {
            if(ticker >= animList[i].timer && !animList[i].triggered)
            {
                animList[i].item.Play(animList[i].animName);
                animList[i].triggered = true;
            }
        }
	}
}

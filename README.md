# README #

A multi-genre game based around three space empires, Siathan Court, Siametric Outreach and Tharian Seekers of Wisdom.

### Info ###

Using C# and the Unity engine, Siathan Archives takes inspiration from many other games. Stellaris being the main one, where the origin of the stories come from as well as Torchlight 2 for the first instance of gameplay.